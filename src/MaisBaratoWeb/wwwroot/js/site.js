﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    if ($('#image-cropper').length > 0) {
        loadCropper();
    }

    if ($('#image-cropper-round').length > 0) {
        loadRoundedCropper();
    }

    if ($('#validation_modal').length > 0) {
        $('#validation_modal').modal('show');
    }
});

function loadCropper() {

    var avatar = document.getElementById('thumb');
    var image = document.getElementById('imageCrop');
    var input = document.getElementById('ImageFile');
    var imageStr = $('#ImageStr');
    var $modal = $('#modal');
    var cropper;

    input.addEventListener('change',
        function(e) {
            var files = e.target.files;
            var done = function(url) {
                input.value = '';
                image.src = url;
                //avatar.src = url;
                //$(avatar).removeClass('hidden');
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

    $modal.on('shown.bs.modal',
        function() {
            cropper = new Cropper(image,
                {
                    aspectRatio: 4/3,
                    viewMode: 0,
                    zoomable: false,
                    autoCropArea: 1,
                });
        }).on('hidden.bs.modal',
        function() {
            cropper.destroy();
            cropper = null;
        });

    document.getElementById('crop').addEventListener('click',
        function() {
            var canvas;

            $modal.modal('hide');

            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: 480,
                    height: 320,
                });
                avatar.src = canvas.toDataURL();
                imageStr.val(canvas.toDataURL());
                $(avatar).removeClass('hidden');
                $('#load').addClass('hidden');
            }
        });

}

function loadRoundedCropper() {

    var avatar = document.getElementById('thumb');
    var image = document.getElementById('imageCrop');
    var input = document.getElementById('ImageFile');
    var imageStr = $('#ImageStr');
    var $modal = $('#modal');
    var cropper;

    input.addEventListener('change',
        function(e) {
            var files = e.target.files;
            var done = function(url) {
                input.value = '';
                image.src = url;
                //avatar.src = url;
                //$(avatar).removeClass('hidden');
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

    $modal.on('shown.bs.modal',
        function() {
            cropper = new Cropper(image,
                {
                    aspectRatio: 1,
                    viewMode: 0,
                    zoomable: true,
                    autoCropArea: 1
                });
        }).on('hidden.bs.modal',
        function() {
            cropper.destroy();
            cropper = null;
        });

    document.getElementById('crop').addEventListener('click',
        function() {
            var canvas;

            $modal.modal('hide');

            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: 480,
                    height: 320
                });

                var roundedCanvas = getRoundedCanvas(canvas);

                avatar.src = roundedCanvas.toDataURL();
                imageStr.val(roundedCanvas.toDataURL());
                $(avatar).removeClass('hidden');
                $('#load').addClass('hidden');
            }
        });

}

function getRoundedCanvas(sourceCanvas) {
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var width = sourceCanvas.width;
    var height = sourceCanvas.height;

    canvas.width = width;
    canvas.height = height;
    context.imageSmoothingEnabled = true;
    context.drawImage(sourceCanvas, 0, 0, width, height);
    context.globalCompositeOperation = 'destination-in';
    context.beginPath();
    context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
    context.fill();
    return canvas;
}

function initAutocomplete() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -15.826691, lng: -47.9218204 },
        zoom: 4,
        mapTypeId: 'roadmap',
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        rotateControl: true
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        console.log(places);

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.splice(0, 1).forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            var marker = new google.maps.Marker({
                map: map,
                title: place.name,
                position: place.geometry.location,
                draggable: true,
            });

            // when dragend, show new lat and lng in console
            google.maps.event.addListener(marker,
                'dragend',
                function() {
                    $("#Localization").val(marker.position.lat() + "," + marker.position.lng());
            });

            // Create a marker for each place.
            markers.push(marker);

            $("#Localization").val(place.geometry.location.lat() + "," + place.geometry.location.lng());

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
}