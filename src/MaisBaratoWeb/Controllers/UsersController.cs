﻿using MaisBaratoWeb.Clients;
using MaisBaratoWeb.Models.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MaisBaratoWeb.Controllers
{
    public class UsersController : Controller
    {
        private UserClient _userClient;
        private UserModel _adminUser;

        public UsersController(UserClient userClient, UserModel adminUser)
        {
            _userClient = userClient;
            _adminUser = adminUser;
        }

        [HttpGet("/[controller]/{userId}/[action]")]
        public async Task<IActionResult> ValidateUser(string userId, [FromQuery(Name = "authToken")] string authToken)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(authToken))
            {
                ViewData["State"] = "Invalid";
                return View();
            }

            var adminUser = await _userClient.Authenticate(_adminUser);

            var user = await _userClient.GetUserById(userId, adminUser.Token);

            if (user == null)
            {
                ViewData["State"] = "Invalid";
                return View();
            }

            if (user.Valid)
            {
                return Redirect("/");
            }

            if (!user.Token.Equals(authToken))
            {
                ViewData["State"] = "Expired";
                return View();
            }

            user.Valid = true;

            this._userClient.Update(user.Id, user);

            ViewData["State"] = "Success";
            return View();
        }
    }
}