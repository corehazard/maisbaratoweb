﻿using MaisBaratoWeb.Clients;
using MaisBaratoWeb.Extensions;
using MaisBaratoWeb.Models.Authentication;
using MaisBaratoWeb.Models.PnP;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MaisBaratoWeb.Controllers
{
    public class OfertasController : Controller
    {
        private ProductClient _client;
        private PlaceModel _place;
        private UserModel _user;
        private readonly IHostingEnvironment _environment;

        public OfertasController(ProductClient client, IHostingEnvironment environment)
        {
            this._client = client;
            _environment = environment;

            this._place = new PlaceModel();

            this._user = new UserModel();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> Index()
        {
            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            var products = await this._client.SearchByPlace(this._place);

            TempData["Changed"] = TempData["Changed"];

            return View(products);
        }

        [HttpGet]
        public async Task<IActionResult> Alterar(string id)
        {
            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            var product = await this._client.GetProductById(id);

            using (var image = Image.Load(Path.Combine(_environment.WebRootPath, "images", "sales", $"{product.Image}")))
            {
                using (var memoryStream = new MemoryStream())
                {
                    image.Save(memoryStream, new PngEncoder());

                    var imageBytes = memoryStream.ToArray();

                    product.Image = $"data:image/png;base64,{Convert.ToBase64String(imageBytes)}";
                }
            }

            return View(product);
        }

        [HttpPost]
        public async Task<IActionResult> Alterar(ProductModel product)
        {

            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            product.Categories = new List<ICategory>();
            product.CategoriesString = product.CategoriesString.Replace(", ", ",");

            foreach (var categoryName in product.CategoriesString.Split(","))
            {
                product.Categories.Add(new CategoryModel { VisualName = categoryName.Trim() });
            }

            product.Status = true;
            product.Timestamp = DateTime.Now;
            product.Price = 0;

            if (ModelState.IsValid)
            {
                var filePath = Path.Combine(_environment.WebRootPath, "images", "sales", $"{product.ImageFileName}");

                var bytes = Convert.FromBase64String(product.Image.Split(',')[1]);

                using (var memoryStream = new FileStream(filePath, FileMode.Create))
                {
                    await memoryStream.WriteAsync(bytes, 0, bytes.Length);

                    product.Images.Clear();
                    product.Images.Add(product.ImageFileName);
                }

                this._client.Update(product.Id, product);
                TempData["Changed"] = $"A oferta '{product.Name}' foi atualizada!";

                return RedirectToAction("Index", "Ofertas");
            }

            return View(product);
        }

        [HttpGet]
        public async Task<IActionResult> Desativar(string id)
        {

            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            var product = await this._client.GetProductById(id);

            product.Status = false;

            this._client.Update(id, product);

            TempData["Changed"] = $"A oferta '{product.Name}' foi desativada!";

            return RedirectToAction("Index", "Ofertas");
        }

        [HttpGet]
        public async Task<IActionResult> Ativar(string id)
        {

            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            var product = await this._client.GetProductById(id);

            product.Status = true;
            product.AvailableUntil = DateTime.Now;

            this._client.Update(id, product);

            TempData["Changed"] = $"A oferta '{product.Name}' foi ativada e o vencimento foi alterado para hoje!";

            return RedirectToAction("Index", "Ofertas");
        }

        [HttpGet]
        public IActionResult NovaOferta()
        {
            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            var partial = new ProductModel() { PlaceId = this._place.Id, AvailableUntil = DateTime.Now };
            return View(partial);
        }

        [HttpPost]
        public async Task<IActionResult> NovaOferta(ProductModel product)
        {

            this._place = HttpContext.Session.GetObject<PlaceModel>("Place");
            this._user = HttpContext.Session.GetObject<UserModel>("User");

            if (this._user == null) return RedirectToAction("Index", "Home");

            ViewData["Username"] = this._user.FirstName;

            product.Categories = new List<ICategory>();
            product.CategoriesString = product.CategoriesString.Replace(", ", ",");

            foreach (var categoryName in product.CategoriesString.Split(","))
            {
                product.Categories.Add(new CategoryModel { VisualName = categoryName });
            }

            product.Status = true;
            product.Timestamp = DateTime.Now;
            product.Price = 0;

            if (ModelState.IsValid)
            {
                var myUniqueFilename = Convert.ToString(Guid.NewGuid()).Replace("-", string.Empty);

                var newFileName = $"{myUniqueFilename}.png";

                var filePath = Path.Combine(_environment.WebRootPath, "images", "sales", $"{newFileName}");

                var bytes = Convert.FromBase64String(product.Image.Split(',')[1]);

                using (var memoryStream = new FileStream(filePath, FileMode.Create))
                {
                    await memoryStream.WriteAsync(bytes, 0, bytes.Length);

                    product.Images.Clear();
                    product.Images.Add(newFileName);
                }

                var createdProduct = await this._client.CreateProduct(product);

                TempData["Changed"] = $"A oferta '{product.Name}' foi criada!";

                return RedirectToAction("Index");
            }

            return View(product);
        }
    }
}