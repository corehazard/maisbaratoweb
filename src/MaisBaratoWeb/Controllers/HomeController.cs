﻿using Hazard.BaseAPI.SDK.Base;
using MaisBaratoWeb.Clients;
using MaisBaratoWeb.Extensions;
using MaisBaratoWeb.Helpers.Converters;
using MaisBaratoWeb.Helpers.Email;
using MaisBaratoWeb.Models.Authentication;
using MaisBaratoWeb.Models.PnP;
using MaisBaratoWeb.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace MaisBaratoWeb.Controllers
{
    public class HomeController : Controller
    {
        private UserClient _userClient;
        private PlaceClient _placeClient;
        private EmailService _emailService;
        private readonly IHostingEnvironment _environment;

        public HomeController(UserClient userClient, PlaceClient placeClient, EmailService emailService, IHostingEnvironment environment)
        {
            this._userClient = userClient;
            this._placeClient = placeClient;
            _emailService = emailService;
            _environment = environment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignUp(UserModel user)
        {
            if (ModelState.IsValid)
            {
                var role = new RoleModel { Name = "Admin" };

                user.DocumentType = "CNPJ";
                user.Role = role;

                var newUser = await _userClient.CreateUser(user);

                var email = new EmailMessage
                {
                    Content = $@"<h2>Bem vindo ao +Barato, {user.FirstName}!</h2>
                        <p>Estamos muito felizes que você deseja ser um de nossos parceiros!</p>

                        <p>Para confirmar o seu cadastro, por favor, basta apenas clicar <a href=""http://maisbaratosim.com.br/users/{newUser.Id}/ValidateUser?authToken={newUser.Token}"">neste link</a>!</p>
                                            
                        <p>Agradecemos pela preferência!</p><br/>

                        <p>Att,</p>

                        <p>Equipe +Barato</p>
                    ",
                    Subject = "+Barato: Seja bem vindo!",
                    ToAddresses = new List<EmailAddress> { new EmailAddress { Address = newUser.Email, Name = $"{newUser.FirstName} {newUser.LastName}" } },
                    FromAddresses = new List<EmailAddress> { new EmailAddress { Address = "contato@maisbaratosim.com.br", Name = "Site MaisBarato" } }
                };

                var teamEmail = new EmailMessage
                {
                    Content = $@"
                        <h2>Um novo parceiro se cadastrou no +Barato!</h2>

                        
                        <p>Segue os dados do novo parceiro:</p>

                        <p>Nome: {user.FirstName} {user.LastName}</p>
                        <p>Email: {user.Email}</p>
                        <p>CNPJ: {user.Document}</p>
                        <p>Nome do Usuário: {user.Username}</p>

                        <p>Att,</p>

                        <p>Site +Barato</p>
                    ",
                    Subject = $"Novo parceiro se cadastrou: {user.FirstName} {user.LastName}!",
                    ToAddresses = new List<EmailAddress> { new EmailAddress { Address = "cesar@maisbaratosim.com.br" }, new EmailAddress { Address = "leticia@maisbaratosim.com.br" }, new EmailAddress { Address = "italo@maisbaratosim.com.br" } },
                    FromAddresses = new List<EmailAddress> { new EmailAddress { Address = "contato@maisbaratosim.com.br", Name = "Site MaisBarato" } }
                };

                this._emailService.Send(email);
                this._emailService.Send(teamEmail);

                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public async Task<IActionResult> CompleteProfile(SignUpViewModel user)
        {
            var loggedUser = HttpContext.Session.GetObject<UserModel>("User");

            if (loggedUser == null) return RedirectToAction("Index", "Home");

            if (ModelState.IsValid)
            {
                var location = new LocationModel()
                {
                    Coordinates = new List<double>(Array.ConvertAll(user.Localization.Split(','), Double.Parse)),
                    Type = "Point"
                };

                var newPlace = new PlaceModel(location)
                {
                    ClaimantId = loggedUser.Id,
                    Name = user.PlaceName
                };

                var myUniqueFilename = Convert.ToString(Guid.NewGuid()).Replace("-", string.Empty);

                var newFileName = $"{myUniqueFilename}.png";

                var filePath = Path.Combine(_environment.WebRootPath, "images", "profiles", $"{newFileName}");

                var bytes = Convert.FromBase64String(user.Image.Split(',')[1]);

                using (var memoryStream = new FileStream(filePath, FileMode.Create))
                {
                    await memoryStream.WriteAsync(bytes, 0, bytes.Length);

                    newPlace.Image = newFileName;
                }

                var createdPlace = await this._placeClient.CreatePlace(newPlace);

                var place = await this._placeClient.GetPlaceById(createdPlace.Id);

                HttpContext.Session.SetObject("Place", place);

                TempData["Changed"] = $"Seja bem vindo {loggedUser.FirstName}! Agora você pode adicionar as suas melhores ofertas aqui, e elas irão aparecer imediatamente no App!";

                return RedirectToAction("Index", "Ofertas");
            }

            return View("SignUp", user);
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserModel user)
        {
            try
            {
                var usr = await this._userClient.Authenticate(user);

                if (usr != null)
                {
                    HttpContext.Session.Clear();
                    HttpContext.Session.SetObject("User", usr);

                    var places = await this._placeClient.Search(new PlaceModel { ClaimantId = usr.Id });

                    var place = places.Find(p => p.ClaimantId.Equals(usr.Id));

                    if (place == null)
                    {
                        ViewData["Username"] = usr.FirstName;
                        return View("SignUp", SignUpConverter.FromUser(usr));
                    }

                    HttpContext.Session.SetObject("Place", place);

                    return RedirectToAction("Index", "Ofertas");
                }
            }
            catch (InvalidCredentialException)
            {
                ViewData["ErrorMessage"] = "Login Inválido";
            }

            StatusCode(401);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}