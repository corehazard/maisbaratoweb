﻿using System.ComponentModel.DataAnnotations;

namespace MaisBaratoWeb.ViewModels
{
    public class PartnerDataViewModel
    {
        [Required]
        public string Name
        {
            get; set;
        }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email
        {
            get; set;
        }

        [Required]
        public string PlaceName
        {
            get; set;
        }
    }
}
