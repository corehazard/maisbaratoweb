﻿using MaisBaratoWeb.Models.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MaisBaratoWeb.ViewModels
{
    public class SignUpViewModel : IUser
    {
        public SignUpViewModel()
        {
            this.Role = new RoleModel();
            this.Images = new List<string>();
        }

        public SignUpViewModel(RoleModel role)
        : this()
        {
            this.Role = role;
        }

        public SignUpViewModel(RoleModel role, List<string> images)
        : this(role)
        {
            this.Images = new List<string>(images);
        }

        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("username")]
        [Display(Name = "Usuário", Prompt = "Usuário")]
        public string Username { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public string Document { get; set; }

        [JsonProperty("document_type")]
        public string DocumentType { get; set; }

        [Display(Name = "Senha", Prompt = "Senha")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public IRole Role { get; set; }
        public string Token { get; set; }
        public bool Valid { get; set; }
        public string PlaceName { get; set; }
        public string Localization { get; set; }
        public IList<string> Images { get; set; }

        [JsonIgnore]
        [Required]
        [RegularExpression(@"^data:image\/png.*", ErrorMessage = "A Imagem deve estar no formato PNG")]
        public string Image { get => Images.Count > 0 ? Images[0] ?? "" : ""; set => Images.Add(value); }

        [JsonIgnore]
        [DisplayName("Imagem do Estabelecimento")]
        public IFormFile ImageFile { get; set; }

        [JsonIgnore]
        public string ImageFileName { get; set; }
    }
}