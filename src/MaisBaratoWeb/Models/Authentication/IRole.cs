﻿namespace MaisBaratoWeb.Models.Authentication
{
    public interface IRole
    {
        string Name { get; set; }
    }
}