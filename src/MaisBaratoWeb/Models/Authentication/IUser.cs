﻿namespace MaisBaratoWeb.Models.Authentication
{
    public interface IUser
    {
        string Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Username { get; set; }
        string Email { get; set; }
        string Document { get; set; }
        string DocumentType { get; set; }
        string Password { get; set; }
        IRole Role { get; set; }
        string Token { get; set; }
        bool Valid { get; set; }
    }
}