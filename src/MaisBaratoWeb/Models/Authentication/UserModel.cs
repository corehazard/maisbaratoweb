﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MaisBaratoWeb.Models.Authentication
{
    public class UserModel : IUser
    {
        public UserModel()
        {
            this.Role = new RoleModel();
        }

        public UserModel(RoleModel role)
        {
            this.Role = role;
        }

        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("username")]
        [Display(Name = "Usuário", Prompt = "Usuário")]
        public string Username { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public string Document { get; set; }

        [JsonProperty("document_type")]
        public string DocumentType { get; set; }

        [Display(Name = "Senha", Prompt = "Senha")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public IRole Role { get; set; }
        public string Token { get; set; }
        public bool Valid { get; set; }
    }
}