﻿using System;
using System.Collections.Generic;

namespace MaisBaratoWeb.Models.PnP
{
    public interface IProduct
    {
        string Id
        {
            get; set;
        }

        string PlaceId
        {
            get; set;
        }

        string Name
        {
            get; set;
        }

        string Brand
        {
            get; set;
        }

        float Price
        {
            get; set;
        }

        string Currency
        {
            get; set;
        }

        IList<ICategory> Categories
        {
            get; set;
        }

        string Description
        {
            get; set;
        }

        bool Status
        {
            get; set;
        }

        DateTime Timestamp
        {
            get; set;
        }

        DateTime AvailableUntil
        {
            get; set;
        }

        IList<string> Images
        {
            get; set;
        }

        bool ContainsAllCategories(IList<ICategory> categories);
    }
}