﻿namespace MaisBaratoWeb.Models.PnP
{
    public interface IPlace
    {
        string Id
        {
            get; set;
        }

        string ClaimantId
        {
            get; set;
        }

        string Name
        {
            get; set;
        }

        string Description
        {
            get; set;
        }

        ILocation Location
        {
            get; set;
        }

        string Image
        {
            get; set;
        }
    }
}
