﻿namespace MaisBaratoWeb.Models.PnP
{
    public interface ICategory
    {
        string Id
        {
            get; set;
        }

        string Name
        {
            get; set;
        }

        string VisualName
        {
            get; set;
        }
    }
}