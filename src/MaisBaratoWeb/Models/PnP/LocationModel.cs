﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MaisBaratoWeb.Models.PnP
{
    public class LocationModel : ILocation
    {
        public string Type { get; set; }
        public IList<double> Coordinates { get; set; }

        [JsonIgnore]
        public double Latitude
        {
            get => Coordinates[0];
            set => Coordinates[0] = value;
        }

        [JsonIgnore]
        public double Longitude
        {
            get => Coordinates[1];
            set => Coordinates[1] = value;
        }
    }
}