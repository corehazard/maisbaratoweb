﻿using System.Collections.Generic;

namespace MaisBaratoWeb.Models.PnP
{
    public interface ILocation
    {
        string Type
        {
            get; set;
        }

        IList<double> Coordinates
        {
            get; set;
        }

        double Latitude
        {
            get; set;
        }

        double Longitude
        {
            get; set;
        }
    }
}