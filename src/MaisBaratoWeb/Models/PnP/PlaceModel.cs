﻿using Newtonsoft.Json;

namespace MaisBaratoWeb.Models.PnP
{
    public class PlaceModel : IPlace
    {
        public PlaceModel()
        {
            this.Location = new LocationModel();
        }

        public PlaceModel(LocationModel location)
            : this()
        {
            this.Location = location;
        }

        public string Id { get; set; }

        [JsonProperty("claimant_id")]
        public string ClaimantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ILocation Location { get; set; }
        public string Image { get; set; }
    }
}