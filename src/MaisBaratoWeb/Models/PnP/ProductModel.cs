﻿using MaisBaratoWeb.Helpers.Converters;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MaisBaratoWeb.Models.PnP
{
    public class ProductModel : IProduct
    {
        public ProductModel()
        {
            this.Images = new List<string>();
            this.AvailableUntil = DateTime.Now;
        }

        public ProductModel(List<CategoryModel> categories, List<string> images)
        {
            this.Categories = new List<ICategory>(categories);
            this.Images = new List<string>(images);
        }

        public string Id { get; set; }

        [JsonProperty("place_id")]
        [Required]
        public string PlaceId { get; set; }

        [DisplayName("Título da Oferta")]
        [Required]
        public string Name { get; set; }
        public string Brand { get; set; }
        public float Price { get; set; }
        public string Currency { get; set; }

        [DisplayName("Categorias")]
        [Required]
        [RegularExpression("([a-zA-Z\\s,çÇáàãâÀÁÃÂéÉíÍóõôÓÕÔúÚüÜ]+)", ErrorMessage = "Apenas palavras separadas por vírgula")]
        [JsonIgnore]
        public string CategoriesString { get; set; }

        [JsonConverter(typeof(ListCategoryConverter))]
        public IList<ICategory> Categories { get; set; }

        [DisplayName("Descrição da Oferta")]
        [Required]
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime Timestamp { get; set; }

        [JsonProperty("available_until")]
        [DisplayName("Válida até")]
        [Required]
        public DateTime AvailableUntil { get; set; }
        public IList<string> Images { get; set; }

        [JsonIgnore]
        [Required]
        [RegularExpression(@"^data:image\/png.*", ErrorMessage = "A Imagem deve estar no formato PNG")]
        public string Image { get => Images.Count > 0 ? Images[0] ?? "" : ""; set => Images.Add(value); }

        public bool ContainsAllCategories(IList<ICategory> categories)
        {
            return !categories.Except(this.Categories).Any();
        }

        [JsonIgnore]
        [DisplayName("Imagem da Oferta")]
        public IFormFile ImageFile { get; set; }

        [JsonIgnore]
        public string ImageFileName { get; set; }
    }
}