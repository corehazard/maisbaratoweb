﻿using Newtonsoft.Json;

namespace MaisBaratoWeb.Models.PnP
{
    public class CategoryModel : ICategory
    {
        public string Id { get; set; }
        public string Name { get; set; }

        [JsonProperty("visual_name")]

        public string VisualName { get; set; }
    }
}