﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hazard.BaseAPI.SDK.Base;
using MaisBaratoWeb.Clients;
using MaisBaratoWeb.Helpers.Email;
using MaisBaratoWeb.Models.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;

namespace MaisBaratoWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                };
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }).AddSessionStateTempDataProvider();
            services.AddSession();

            services.AddSingleton<IEmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
            services.AddTransient<IEmailService, EmailService>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var apiConfiguration = this.Configuration.GetSection("API");
            var authConfiguration = this.Configuration.GetSection("AuthAPI");

            var apiUrl = apiConfiguration["Url"] ?? Environment.GetEnvironmentVariable("PNP_BASE_URI");
            var authUrl = authConfiguration["Url"] ?? Environment.GetEnvironmentVariable("AUTH_API_URI");

            if (string.IsNullOrEmpty(apiUrl))
            {
                throw new InvalidDataException("You must configure a URI for the API.");
            }

            if (string.IsNullOrEmpty(apiUrl))
            {
                throw new InvalidDataException("You must configure a URI for the Auth API.");
            }

            var configuration = Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();

            var productClient = new ProductClient(new Uri(apiUrl));
            var placeClient = new PlaceClient(new Uri(apiUrl));
            var userClient = new UserClient(new Uri(authUrl));
            var emailService = new EmailService(configuration);

            var adminUser = Configuration.GetSection("AdminUser").Get<UserModel>();

            var authToken = "";

            var builder = new Autofac.ContainerBuilder();

            builder.RegisterInstance(productClient).As<ProductClient>();
            builder.RegisterInstance(placeClient).As<PlaceClient>();
            builder.RegisterInstance(userClient).As<UserClient>();
            builder.RegisterInstance(authToken).As<string>();
            builder.RegisterInstance(adminUser).As<UserModel>();
            builder.RegisterInstance(emailService).As<EmailService>();

            builder.Populate(services);

            var container = builder.Build();

            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSession(new SessionOptions { IdleTimeout = TimeSpan.FromHours(12), IOTimeout = TimeSpan.FromHours(12) });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
