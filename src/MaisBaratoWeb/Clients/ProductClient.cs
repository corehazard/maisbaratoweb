﻿using MaisBaratoWeb.Models.PnP;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaisBaratoWeb.Clients
{
    public class ProductClient : ClientBase
    {
        public ProductClient(Uri baseEndpoint) : base(baseEndpoint) { }

        public async Task<ProductModel> CreateProduct(ProductModel product)
        {
            var requestUri = this.CreateRequestUri("/api/products");
            var requestContent = this.CreateHttpContent(product);
            var response = await this._client.PostAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<ProductModel>(data);
        }

        public async Task<ProductModel> GetProductById(string id)
        {
            var requestUri = this.CreateRequestUri($"/api/products/{id}");
            var response = await this._client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<ProductModel>(data);
        }

        public async Task<List<ProductModel>> GetAllProducts()
        {
            var requestUri = this.CreateRequestUri("/api/products/_allProducts");
            var response = await this._client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<ProductModel>>(data);
        }

        public async Task<List<ProductModel>> Search(ProductModel product)
        {
            var requestUri = this.CreateRequestUri("/api/products/_search");
            var requestContent = this.CreateHttpContent(product);
            var response = await this._client.PostAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<ProductModel>>(data);
        }

        public async Task<List<ProductModel>> SearchByPlace(PlaceModel place)
        {
            var requestUri = this.CreateRequestUri("/api/products/_place");
            var requestContent = this.CreateHttpContent(place);
            var response = await this._client.PostAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<ProductModel>>(data);
        }

        public async void Update(string id, ProductModel product)
        {
            var requestUri = this.CreateRequestUri($"/api/products/{id}");
            var requestContent = this.CreateHttpContent(product);
            var response = await this._client.PutAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();
        }

        public async void Delete(string id)
        {
            var requestUri = this.CreateRequestUri($"/api/products/{id}");
            var response = await this._client.DeleteAsync(requestUri);
            response.EnsureSuccessStatusCode();
        }
    }
}