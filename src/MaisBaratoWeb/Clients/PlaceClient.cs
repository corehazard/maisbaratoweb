﻿using MaisBaratoWeb.Clients;
using MaisBaratoWeb.Models.PnP;
using Microsoft.CodeAnalysis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hazard.BaseAPI.SDK.Base
{
    public class PlaceClient : ClientBase
    {
        public PlaceClient(Uri baseEndpoint) : base(baseEndpoint) { }

        public async Task<PlaceModel> CreatePlace(PlaceModel place)
        {
            var requestUri = this.CreateRequestUri("/api/places");
            var requestContent = this.CreateHttpContent(place);
            var response = await this._client.PostAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<PlaceModel>(data);
        }

        public async Task<PlaceModel> GetPlaceById(string id)
        {
            var requestUri = this.CreateRequestUri($"/api/places/{id}");
            var response = await this._client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<PlaceModel>(data);
        }

        public async Task<List<PlaceModel>> GetAllPlaces()
        {
            var requestUri = this.CreateRequestUri("/api/places/_allPlaces");
            var response = await this._client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<PlaceModel>>(data);
        }

        public async Task<List<PlaceModel>> Search(PlaceModel place)
        {
            var requestUri = this.CreateRequestUri("/api/places/_search");
            var requestContent = this.CreateHttpContent(place);
            var response = await this._client.PostAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<PlaceModel>>(data);
        }

        public async Task<List<PlaceModel>> SearchNear(Location location, int range)
        {
            var requestUri = this.CreateRequestUri("/api/places/_near", $"range={range}");
            var requestContent = this.CreateHttpContent(location);
            var response = await this._client.PostAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<PlaceModel>>(data);
        }

        public async void Update(string id, PlaceModel place)
        {
            var requestUri = this.CreateRequestUri($"/api/places/{id}");
            var requestContent = this.CreateHttpContent(place);
            var response = await this._client.PutAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();
        }

        public async void Delete(string id)
        {
            var requestUri = this.CreateRequestUri($"/api/places/id={id}");
            var response = await this._client.DeleteAsync(requestUri);
            response.EnsureSuccessStatusCode();
        }
    }
}