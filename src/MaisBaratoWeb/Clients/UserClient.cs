﻿using MaisBaratoWeb.Models.Authentication;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace MaisBaratoWeb.Clients
{
    public class UserClient : ClientBase
    {
        public UserClient(Uri baseEndpoint) : base(baseEndpoint) { }

        public async Task<UserModel> CreateUser(UserModel user)
        {
            var requestUri = this.CreateRequestUri("/api/users");
            var requestContent = this.CreateHttpContent(user);
            var response = await this._client.PostAsync(requestUri, requestContent);

            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception)
            {
                throw new UnauthorizedAccessException("You haven't the right access to create an user.");
            }

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<UserModel>(data);
        }

        public async Task<UserModel> Authenticate(UserModel user)
        {
            var requestUri = this.CreateRequestUri("/api/users/authenticate");
            var requestContent = this.CreateHttpContent(user);
            var response = await this._client.PostAsync(requestUri, requestContent);

            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception)
            {
                throw new InvalidCredentialException("Invalid username or password.");
            }

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<UserModel>(data);
        }

        public async Task<UserModel> GetUserById(string id, string accessToken)
        {
            var requestUri = this.CreateRequestUri($"/api/users/{id}");
            this._client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await this._client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<UserModel>(data);
        }

        public async Task<bool> IsAuthenticated(string id, string authToken)
        {
            var requestUri = this.CreateRequestUri($"/api/users/{id}");
            this._client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", authToken);

            var response = await this._client.GetAsync(requestUri);

            if (response.StatusCode == HttpStatusCode.Unauthorized) return false;

            return true;
        }

        public async void Update(string id, UserModel user)
        {
            var requestUri = this.CreateRequestUri($"/api/users/{id}");
            this._client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", user.Token);

            var requestContent = this.CreateHttpContent(user);

            var response = await this._client.PutAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();
        }
        public async void ChangePassword(string id, UserModel user)
        {
            var requestUri = this.CreateRequestUri($"/api/users/{id}/change_password");
            this._client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", user.Token);

            var requestContent = this.CreateHttpContent(user);

            var response = await this._client.PutAsync(requestUri, requestContent);
            response.EnsureSuccessStatusCode();
        }
    }
}