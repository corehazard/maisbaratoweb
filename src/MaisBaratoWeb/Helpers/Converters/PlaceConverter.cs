﻿using MaisBaratoWeb.Models.PnP;
using Newtonsoft.Json;
using System;

namespace MaisBaratoWeb.Helpers.Converters
{
    public class PlaceConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => objectType == typeof(IPlace);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize(reader, typeof(PlaceModel));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(PlaceModel));
        }
    }
}