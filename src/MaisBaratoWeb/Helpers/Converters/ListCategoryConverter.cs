﻿using MaisBaratoWeb.Models.PnP;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace MaisBaratoWeb.Helpers.Converters
{
    public class ListCategoryConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => objectType == typeof(List<ICategory>);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonArray = JArray.Load(reader);
            var deserialized = new List<CategoryModel>();
            serializer.Populate(jsonArray.CreateReader(), deserialized);
            return new List<ICategory>(deserialized);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(List<CategoryModel>));
        }
    }
}