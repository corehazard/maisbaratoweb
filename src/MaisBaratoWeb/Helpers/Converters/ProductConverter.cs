﻿using MaisBaratoWeb.Models.PnP;
using Newtonsoft.Json;
using System;

namespace MaisBaratoWeb.Helpers.Converters
{
    public class ProductConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => objectType == typeof(IProduct) || objectType == typeof(ProductModel);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize(reader, typeof(ProductModel));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(ProductModel));
        }
    }
}