﻿using MaisBaratoWeb.Models.Authentication;
using MaisBaratoWeb.ViewModels;

namespace MaisBaratoWeb.Helpers.Converters
{
    public static class SignUpConverter
    {
        public static SignUpViewModel FromUser(UserModel user)
        {
            var convertedUser = new SignUpViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Document = user.Document,
                DocumentType = user.DocumentType,
                Email = user.Email,
                Id = user.Id,
                Role = user.Role,
                Password = user.Password,
                Username = user.Username,
                Token = user.Token
            };


            return convertedUser;
        }
    }
}