﻿using System.Collections.Generic;

namespace MaisBaratoWeb.Helpers.Email
{
    public interface IEmailService
    {
        void Send(EmailMessage emailMessage);
        List<EmailMessage> ReceiveEmail(int maxCount = 10);
    }
}